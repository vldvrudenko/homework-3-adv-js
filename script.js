"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;

    }

    set age(age) {
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer  extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary * 3;
    }
}

const worker1 = new Programmer ('Vova', '21', '2000', ["JAVA,JS"]);
console.log(worker1);

const worker2 = new Programmer ('Vasia', '16', '3000', ["PHP,JAVA"]);
console.log(worker2);

const worker3 = new Programmer ('Vitia', '47', '4000', ["JS,Phyton"]);
console.log(worker3);
